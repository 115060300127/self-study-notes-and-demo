## iframe

https://www.cnblogs.com/lvhw/p/7107436.html

```javascript
iframe.height = window.parent.document.body.clientHeight;
//获取浏览器元素的宽高
 iframe.width = window.parent.document.body.clientWidth;

//js设置iframe元素的宽度
document.getElementById('navv').width="138";
```

只改变input光标的颜色而不改变字体的颜色那就使用**caret-color**属性:

```css
/*两种方法都可以只改变input光标的颜色*/

/*方法1：点击input框出现光标，默认文字不会消失*/
input {
	caret-color: red;
}
/*方法2：点击input框出现光标，默认文字消失*/
input:focus {
	color:red;
}
```



```js
//处理从后台传递过来的数据
//https://blog.csdn.net/you18131371836/article/details/72466234

var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if(this.readyState==4 && this.status==200) {
                // 将后台返回的字符串数据转js对象
                myobj=JSON.parse(this.responseText);
                // 对数据进行处理
                $(document).ready(function() {
                    $.ajax({
                        url: '',
                        async: 'yes',
                        type: 'Post',
                        dataType: 'json',
                        success: function(data) {
                            creatmenu(data);
                        },error:function(data) {
                            alert("error:"+JSON.stringify(data));
                        }
                    })
                });
            }
        }
```

