main.js

​     **1、** **main.js 程序入口文件，是初始化vue实例并使用需要的插件,加载各种公共组件.**

```
import Vue from 'vue'
import App from './App'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
```



 **2.import from 是干嘛的呢？**实际上，importfrom是ES6语法里面的。

1.在main.js的用法是：

```
import Vue from 'vue';
```

其实最完整的写法是：　因为main.js是在src文件中，所以../向前一级相对目录查找node_modules，再依次寻找后面的文件。

　　　　　　　　　　　　　　![img](https://images2018.cnblogs.com/blog/1055766/201803/1055766-20180322171526300-50205486.png)

```
import Vue from "../node_modules/vue/dist/vue.js";
```

 　　　

 2.

```
import App from './App';
```

　　　　其实最完整的写法是：意思其实就是引入我们写好的.vue文件。

```
import App from './App.vue';
```

　3.

```
import router from './route';    ---------      import router from './route.js';
import axios from 'axios';     --------      import axios from '..\node_modules\axios\dist\axios.js';
import './less/index';      --------      import './less/index.less';
```

**1.import...from...的from命令后面可以跟很多路径格式，若只给出vue，axios这样的包名，则会自动到node_modules中加载；若给出相对路径及文件前缀，则到指定位置寻找。**

**2.可以加载各种各样的文件：.js、.vue、.less等等。**

**3.可以省略掉from直接引入。**





**App.vue是我们的主组件，页面入口文件 ，所有页面都是在App.vue下进行切换的。也是整个项目的关键，app.vue负责构建定义及页面组件归集。**



**router index.js 把准备好路由组件注册到路由里**



其他文件的意思：

1. index.html文件入口
2. src放置组件和入口文件
3. node_modules为依赖的模块
4. config中配置了路径端口值等
5. build中配置了webpack的基本配置、开发环境配置、生产环境配置等