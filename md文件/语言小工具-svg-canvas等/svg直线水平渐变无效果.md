svg  直线水平渐变  为什么水平直线的渐变没有效果，必须得是一条倾斜的直线才有渐变效果呢？？

https://blog.csdn.net/u012260672/article/details/80905631

对x1=x2（没有宽度）或者y1=y2（没有高度）的直线（line以及path，如果，stroke里使用的是渐变效果，那么，在各种浏览器上都会出现同一个BUG，这条线消失了。**关键字objectBoundingBox这玩意儿，在元素没有宽度或者高度的时候，会失去作用**。linearGradient渐变又依赖这个属性，所以失效了。

**解决方案很简单，为linearGradient加上属性gradientUnits=”userSpaceOnUse”**

gradientUnits是用于规定元素的坐标系统的，有两个属性userSpaceOnUse和objectBoundingBox，后者是默认的。

```html
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="499" height="2">
     <defs>
         <!-- 水平渐变 -->
         <linearGradient id="grad" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse">
               <stop offset="0%" stop-color="red" stop-opacity="1" />
               <stop offset="100%" style="stop-color:yellow;stop-opacity:1;" />
         </linearGradient>
     </defs>
     <!-- 直线水平渐变 -->
     <line x1="0" y1="0" x2="499" y2="0"  style="stroke-width:2;" stroke="url(#grad)"/>
</svg>


<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="499" height="2">
     <defs>
         <!-- 水平渐变 -->
         <linearGradient id="grad" x1="0%" y1="0%" x2="100%" y2="0%">
               <stop offset="0%" stop-color="red" stop-opacity="1" />
               <stop offset="100%" style="stop-color:yellow;stop-opacity:1;" />
         </linearGradient>
     </defs>
     <!-- 直线水平渐变 -->
    <!-- 如果没有 gradientUnits="userSpaceOnUse"属性，x1=x2或者y1=y2会消失线条，只可以画斜线的渐变   -->
     <line x1="0" y1="1" x2="499" y2="2"  style="stroke-width:2;" stroke="url(#grad)"/>
</svg>
```

