为了防止页面重新自动加载，可以给a标签设置href="javascript:void(0);"

```html
<a href="javascript:void(0);"></a>
<!--按照格式要求，此处的0不能省略!!
	虽然省略看上去也没什么影响。但是当发生点击事件的时候，
	就会报错： Uncaught SyntaxError: Unexpected token )   -->
<!--或者像下面这样： -->
<a href="javascript:;"></a>
```

