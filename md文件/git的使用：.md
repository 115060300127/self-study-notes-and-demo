创建新分支，将diamagnetic推到新分支  合并分支等 

https://blog.csdn.net/cookie3397/article/details/79440235

https://blog.csdn.net/gang456789/article/details/80676073



git的使用： 在项目目录右键打开git bash here

1 初始化git仓库  **git init**      这个仓库会存放git对项目代码进行备份的文件

2 git备份  在git中设置下当前使用git的用户是谁  --global表示全局配置

​		git config --global user.name "677" 回车  设置用户名

​		git config --global user.email "邮箱"    设置邮箱

​		每一次备份都会存储备份者的信息

3 把代码存储到.git仓库中，

​		**git add ./文件名**             把代码放到仓库门口   暂存代码到本地

​		**git commit -m "必须要说明提交了什么内容"**    提交代码，把代码放到房间里    -c表示要添加说明，-m表示说明，""

4 在github官网，创建项目，

5 生成公钥   git  公钥配置  https://www.cnblogs.com/yangshifu/p/9919817.html 

6 将本地仓库关联到github   **git remote add origin 线上仓库地址**

7 **git pull origin master** 

8 最后1步，上传代码到远程仓库  **git push -u origin master**

**二次提交修改后的代码：**

1 git status  表示查看当前代码有没有被放进仓库    绿色表示修改过的代码，红色表示新创建的，白色表示已暂存但未提交的



*【注意 ！！！】*

git pull origin master的时候遇到报错  无法访问到远程仓库，连接后从代理接收到404错误
fatal: unable to access 'https://github.com/677000/permission/': Received HTTP code 404 from proxy after CONNECT

解决方法：

取消代理：

**git config --global --unset http.proxy** 

**git config --global --unset https.proxy** 

重置代理

**git config --global http.proxy http://127.0.0.1:1080**

**git config --global https.proxy http://127.0.0.1:1080**



致命：无法访问“https://github.com/677000/permission/”：代理连接已中止

fatal: unable to access 'https://github.com/677000/permission/': Proxy CONNECT aborted



致命：无法访问“https://github.com/677000/permission/”：连接后从代理接收到HTTP代码404

fatal: unable to access 'https://github.com/677000/permission/': Received HTTP code 404 from proxy after CONNECT



```
error: failed to push some refs to 'git@github.com:kangvcar/Results-Systems--PHP.git'
原因： 
GitHub远程仓库中的README.md文件不在本地仓库中。
解决方案1 
git push -f
```

```
`[root@linux1 qimo]``# git pull --rebase origin master``[root@linux1 qimo]``# git push -u origin master`
```

解决方法参考：https://www.cnblogs.com/xiaozhaoboke/p/11406457.html

# Git 修改 proxy，解决代理导致的代码无法 push 或 pull 的问题   https://www.cnblogs.com/pengdonglin137/articles/10261914.html

```
// 查看当前代理设置
git config --global http.proxy
git config --global https.proxy

// 设置当前代理为 http://127.0.0.1:1080 或 socket5://127.0.0.1:1080
git config --global http.proxy 'http://127.0.0.1:1080'
git config --global https.proxy 'http://127.0.0.1:1080'

git config --global http.proxy 'socks5://127.0.0.1:1080'
git config --global https.proxy 'socks5://127.0.0.1:1080'

// 删除 proxy
git config --global --unset http.proxy
git config --global --unset https.proxy
```

# git使用报错: fatal: Couldn't find remote ref master的解决方法   https://www.cnblogs.com/tig666666/p/8425865.html

### 解决方法有以下几种：

0.如果是新建的仓库（ repositories ）的话在pull代码的时候，出现这个提示，可以忽略不计，直接提交就可以。

1.检查本地GIT的配置

```
git config user.name/git config --global user.name

git config user.email/git config --gloabl user.email
```

使用以上命令来检查本地的用户名和邮箱是否填写正确

2.检查远程仓库配置

```
git remote -v
```

如果远程仓库信息有误，则删除本地仓库配置，并且设置相关地址

```
git remote rm origin
git remote add origin XXXX
```

3.还是不行的话可以找到文件路径下 git文件所在，打开config文件，删除[remote "origin"] 下信息。重复1，2步骤。



esc退出   :q!  终止



参考： https://www.cnblogs.com/mei0619/p/8260696.html







  