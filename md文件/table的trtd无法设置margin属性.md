# table的trtd无法设置margin属性

**我们可以对表格table设置margin，而不能设置padding;**

**对单元格  td  tr 可以设置padding，设置margin无效。**

**对表格table设置border-spacing(IE不支持)**
　　先让我们简单了解一下border-spacing属性。**border-spacing:length;设置单元格td的边在横向和纵向上的间距**。当您指定一个length值时，这个值将作用于横向和纵向上的间距;当您指定两个length值时，第一个将作用于横向间距，第二个值将作用于纵向间距。

## table  { border-spacing :  5px  10px;  }