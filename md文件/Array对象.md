# Array对象：

## 1，遍历数组，返回新数组，不改变原始数组：

#### a,filter()  遍历数组，检测数组种的每个元素是否符合条件。符合条件的元素组成新数组返回。**返回新数组。**

* 如果数组中的元素不满足条件，会继续检测剩余的元素。直到所有元素检测完毕。

* 会创建一个新数组返回

* 不会对空数组进行检测

* 不会改变原始数组

  ```javascript
  var arr = [1,2,3];
  var data1 = arr.filter(el => {
      return el > 1;
  });
  console.log(data1);//[2,3]
  
  //另一种写法：
  function check(el) {
      return el > 1;
  }
  var data2 = arr.filter(check)
  console.log(data2);//[2,3]
  ```

#### b,from()  遍历数组元素，或者遍历Object，处理元素**返回新数组。**

* 会创建一个新数组返回

* 不会对空数组进行检测

* 不会改变原始数组

  ```javascript
  Array.from(object, Function, thisValue)
  //thisValue是函数Function的this对象，可选参数。Function也是可选参数
  
  let arr = Array.from([1,2,3],el => {
      el * 3;
  })//arr => [3,6,9]
  ```

#### c,keys()   遍历数组的可迭代对象，并**返回新数组**。Object对象也可以调用.keys()  。？

```javascript
var arr = ['a', 'b', 'c'];
console.log(Object.keys(arr)); // console: ['0', '1', '2']
```

#### d,map()  遍历数组，通过调用函数处理元素，处理后的元素组成新数组返回。**返回新数组。**

* 会创建一个新数组返回
* 不会对空数组进行检测
* 不会改变原始数组

```javascript
array.map(function(currentValue,index,arr), thisValue)
//arr参数可选。表示当前元素currentValue属于的数组对象

let newarr = [1,2,3].map( (el,index) => {
	el*index;
});//newarr => [0,2,6]
```

#### e,

* 
* 

## 2，会改变原始数组：

* #### forEach() 遍历数组，数组中所有元素都会被检测。

  * 不会检测空数组
  * 会改变原始数组。

* #### pop()   删除数组中最后一个元素，返回删除的元素。

  - 会改变原始数组 

    ```javascript
    let arr = [1,2,3];
    let item = arr.pop();
    
    //item => 3
    //arr => [1,2]
    ```

- push()   从数组的末尾添加一个或者多个元素，并返回添加新元素后的数组。

  - 会改变原始数组 

    ```javascript
    let arr = [1,2];
    arr.push(4,5);
    
    //arr => [1,2,4,5]
    ```

    

## 3，可以用来深拷贝数据的数组方法：

* concat()  

  * 用来连接两个或多个数组，并返回新数组。
  * 该方法不会改变原始数组

  ```javascript
  arr1.concat(arr2,arr3,arr4)
  
  function fn(m,n) {//返回一个长度为m，元素均为n的数组。  ?concat可以拼接非数组吗？？
      return m>0 ? fn(m-1,n).concat(n) : [];
  }
  fn(3,1)//[1,1,1]
  ```

  

## 4，遍历数组，返回的数据类型不是数组。且如果有元素符合条件，剩余元素不再检测。

* #### every()  检测数组的每个元素是否都符合某个条件。**返回布尔值。**

  - 如果数组中检测到有一个元素不符合条件，则会直接返回false且剩余元素不再进行检测。

  - 如果数组中所有元素都符合条件才会返回true

  - every不会检测空数组

  - 不会改变原始数组

    ```javascript
    var arr = [1,2,3];
    var data1 = arr.every(el => {
        return el >= 2;
    })
    console.log(data1);//false
    
    //另一个种写法
    function check(item) {
        return item >= 2;
    }
    var data2 = arr.every(check);
    console.log(data2);//false
    ```

* #### find()  遍历数组，返回第一个符合条件的元素。**返回的是元素，不是数组。**

  - 如果数组中有一个元素满足条件时，find()会返回符合该条件的第一个元素，剩余元素不再进行检测。

  - 如果遍历完所有元素，不存在符合条件的元素时，返回undefined

  - find不会检测空数组

  - find不会改变原始数组

    ```javascript
    var arr = [1,2,3];
    var data1 = arr.find(el => {
        return el > 1;
    });
    console.log(data1);// 2
    
    //另一种写法：
    function check(el) {
        return el > 1;
    }
    var data2 = arr.find(check)
    console.log(data2);// 2
    ```

* #### findIndex()  遍历数组，返回第一个符合条件的元素的索引号。**返回索引号。** 

  - 如果数组中有一个元素满足条件时，剩余元素不再进行检测。

  - 如果遍历完所有元素，都不存在符合条件的元素时，返回 -1 。

  - findIndex不会检测空数组

  - findIndex不会改变原始数组

    ```javascript
    var arr = [1,2,3];
    var data1 = arr.findIndex(el => {
        return el > 3;
    });
    console.log(data1);// -1
    
    //另一种写法：
    function check(el) {
        return el >= 2;
    }
    var data2 = arr.find(check)
    console.log(data2);// 1
    ```

* #### includes()  判断数组中是否包含某个指定值，**返回布尔值。**

  * includes(el,fromIndex)  el为必需参数，表示需要查找的元素值。fromIndex为可选参数，表示从该索引处开始查找。如果fromIndex > arr.length，会直接返回false，数组不会被检测。如果计算出的索引 < 0，整个数组都会被检测。
  * includes不会检测空数组
  * includes不会改变原始数组

  ```javascript
  [1,2,3].includes(2);//true
  
  [1,2,3].includes('2');//false
  
  ['1','2','3'].includes('2');//true
  ```

* #### indexOf()  判断数组中是否包含某个指定值。返回数组中某个指定的元素第一次出现的位置的索引。**返回索引号。**

  * 如果数组中有一个元素满足条件时，剩余元素不再进行检测。

  * 如果遍历完所有元素，都不包含某个指定值，则返回 -1 。

  * 如果 arr.indexOf(item) != -1  , 表示数组arr包含至少一个元素，值为item

  * **也可以用来判断字符串中是否包含某个子串**

    ```javascript
    [1,2,4,1].indexOf(1) // 0  检测到包含1的元素就直接返回相应元素索引号，剩余元素不检测
    [1,2,4,1].indexOf(1,2) // 3  indexOf(item,start)  start为可选参数，表示从索引号start开始检测。
    
    [1,2,4].indexOf(3) // -1  遍历完所有元素都不符合条件，返回 -1 
    
    var str = 'drawstar';
    str.indexOf('star') != -1 //true   
    ```

* #### lastIndexOf()  判断数组中是否包含某个指定值，并**返回它最后一次出现的索引。**

  * 该方法是**从尾到头**进行检测。如果数组中找到元素满足条件时，剩余元素不会检测。返回最后一次的索引。

  * 如果遍历完所有元素，都不包含某个指定值，则返回 -1 。

  * 如果 arr.lastIndexOf(item) != -1  , 表示数组arr包含至少一个元素，值为item

    ```javascript
    [3,1,3,6].lastIndexOf(3);//3  从尾到头检测，先检测6，再检测到元素3符合条件，直接返回该索引号。剩余元素不进行检测。
    ```

* #### isArray()  判断一个数据对象是否是数组类型。**返回布尔值。**

  ```javascript
  var arr = [1,2,4];
  let bool = Array.isArray(arr);//bool => true
  ```

* #### join()  遍历数组元素，将数组所有元素转换成一个字符串。**返回一个字符串。**

  ```javascript
  array.join(separator)
  //separator是可选参数。表示指定要使用的分隔符
  
  let str1 = [1,2,3].join();  //str1  ->  '1,2,3'
  let str2 = [1,2,3].join('-');  //str2  ->  '1-2-3'
  ```

* - 























