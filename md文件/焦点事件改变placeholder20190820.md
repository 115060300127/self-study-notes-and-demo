### **获得焦点清空placeholder:**

```js
			// input框获得焦点时，清空placeholder
            $('input').focus(function() {
                    $(this).prop("placeholder","");
                    $(this).prop("background",green);
            });
            // input框失去焦点时，设置placeholder
            $('input').blur(function() {
                    $(this).prop("placeholder",'添加todo');
            });

```



```html
<input type="text" placeholder="添加todo" class="todoinput" οnfοcus="this.placeholder=''" οnblur="this.placeholder='添加todo'" >
```



- [ ] ### **input获得鼠标焦点时，外边框不变蓝：**outline-style:none;

- [ ] border 可应用于几乎所有有形的html元素，而outline 是针对链接、表单控件和ImageMap等元素设计。从而另一个区别也可以推理出，那就是：outline的效果将随元素的 focus 而自动出现，相应的由 blur 而自动消失。这些都是浏览器的默认行为，无需JavaScript配合CSS来控制。`outline` 不会象`border`那样影响元素的尺寸或者位置。

- [ ] 使用input:focus{outline:none;},可以达到“输入框”获得焦点时边框不变蓝的效果。这个效果也可以使用在button上，button:focus{outline:none;}。

- [ ] 但是在Firefox下，对于类型为button的元素，即使设置outline，也还是会出现虚线。可以使用一个Firefox的私有伪元素button:-moz-focus-inner{border:0;}，特别注意的是-moz-focus-inner特别注意的是reset outline,而是设置border。



split("-")  以什么符号分割字符串    splice(index,个数) 从某处索引下标开始删除数组个数



inherit  initial



## 数据持久化：也叫做数据本地化，是一种将数据长久地保存在客户端的操作。

- 作用 ：避免登录网站时，用户在页面浏览时重复登录，也可以实现快速登录，一段时间内保存用户的登录效果，提高页面访问速率
- 在html5中提供三种数据持久化操作的方法：
- 1.cookie  可看作是记录简单内容的文本文件，直接绑定在html页面上。有前端设置和后端设置
- 写入cookie：document.cookie = 'key=value;expires = 在页面中保存的时间/过期时间戳';
- 读取cookie：   console.log(document.cookie);
- 删除cookie：   document.cookie = 'key=value;expires=当前时间戳+1';   任何方式获取到的时间戳都是要给Date对象
- 2.localStorage
- 3.sessionStorage



## html5  iframe元素：用于在网页中嵌入另一个页面，或嵌入视频  可替换元素 显示内容取决于元素属性  css不能完全控制其中样式    通常是行内块盒子     

target="_blank"在新窗口中打开页面

```html
<a href="" target="myframe"></a>
<iframe name="myframe" src="" scrolling="no" frameborder="no" framespacing="0" allowfullscreen="true"></iframe>
```

# input --checked: 

 只要复选框有checked属性，不管属性值为空或者为true or  false或任意值，复选框都会被选中。切忌：checked属性值不要带引号

```html
<input type="checkbox" checked>1
<input type="checkbox" checked="">2
<input type="checkbox" checked="true">3
<input type="checkbox" checked="false">4
<!--4个复选框都会被勾选 -->
```

