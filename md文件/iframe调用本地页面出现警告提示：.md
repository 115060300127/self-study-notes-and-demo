**iframe调用本地页面出现警告提示： Uncaught DOMException: Blocked a frame with origin "null" from accessing a cross-origin frame.---阻止原点为“空”的帧访问交叉原点帧 【 阻止了一个域为null的frame页面访问另一个域为null的页面。】**

当出现警告的时候，设置的iframe宽高就会失效

原因：代码运行时在本地直接用浏览器打开的，地址栏是file:///的页面                     跨域问题  

解决方法：

1 vscode下载live server插件 ；   ------------不能解决

2 使用nginx搭建服务器，放置到服务器中即可；

3 使用监听方式，将子页面的数据发送给父页面，当父页面接收到数据后执行操作---------解决无法调用父页面的操作；

```javascript
子页面：
var message = {s:"close",filePath:filePath}
                window.parent.postMessage(message, '*');
父页面：
window.addEventListener('message', function (e) {
            //let target = e.target;
            let data = e.data;
            console.log(data);
            if (true)
            {
             	//****
            }           
        }, false);
```



4：https://www.cnblogs.com/PengRay0221/p/10429350.html

launch.json里面加上配置：

　　　　　　,"runtimeArgs": [

　　　　　　　　" --disable-web-security"

　　　　　　]

完整配置文件如下：但这种方法并不安全，仅用于本地测试，测试的时候可以忽略错误。搭好服务器就没有这个问题。

```json
 1     "version": "0.2.0",
 2     "configurations": [
 3         {
 4             "name": "使用本机 Chrome 调试",
 5             "type": "chrome",
 6             "request": "launch",
 7             "file": "${workspaceRoot}/index.html",
 8             //"url": "http://mysite.com/index.html", //使用外部服务器时,请注释掉 file, 改用 url, 并将 useBuildInServer 设置为 false "http://mysite.com/index.html
 9             "runtimeExecutable": "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe", // 改成您的 Chrome 安装路径
10             "sourceMaps": false,
11             "webRoot": "${workspaceRoot}",
12         //  "preLaunchTask":"build",
13             "userDataDir":"${tmpdir}",
14             "port":5433
15             ,"runtimeArgs": [  
16                 " --disable-web-security"   //跨域访问，不安全，仅本地测试
17             ]
18         }
19     ]
```

5 