# javascript	

javascript是由浏览器解释执行；是一种基于对象的语言；

Table 1: Html 对象的常用事件列举
事件名称 触发时间 对象例举
OnBlur 对象失去输入焦点 窗口和所有的表单对象
OnChange 用户改变对象的值 文本框、文本区域、选择列表等
OnClick 用户鼠标点击 链接、按钮、单选钮、多选钮等
OnFocus 获得输入焦点时 窗口和所有的表单对象
OnKeyDown 用户按下一个键时 表单对象，比如输入框、文本区域等
OnKeyUp 用户释放一个键时 表单对象，比如输入框、文本区域等

OnMouseDown 用户按下鼠标时 文档，按钮、链接、图像、表格等
OnMouseOut 鼠标从某个对象移开时 文档，按钮、链接、图像、表格等
OnMouseOver 鼠标移到某个对象 文档，按钮、链接、图像、表格OnReset 表单复位时 表单
OnResize 窗口尺寸变化时 窗口
OnSubmit 表单提交时 表单

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        input.btn {
            background: red;
        }
        .overstyle {
            background: olive;
        }
        .outstyle {
            background: red;
        }
    </style>
    <!-- // js函数代码【建议】嵌入头部之间。在页面文档主体和其余部分代码加载之前【web内容在浏览器中是从上到下解析的，head内的脚本会比body内的脚本线处理。所以最好将包含所有预定义函数的js代码放在头部】 ;js区分大小写；-->
    <script>
        document.write("<font>js代码输出的文字;一般不这么用</font>");//document.write()是文档对象的输出函数。其功能是将括号中的内容或变量值输出到页面文档中。

        //  一些比较简短的js事件代码可以直接写在html标签内，格式：  onclick=javascript:函数名称()   具体的函数写在嵌入head的js代码中
        function submit() {
            alert('提交');
        }

    </script>
</head>
<!-- onload当网页下载到浏览器就会执行，如果onload需要访问html标签所定义的对象，需要确保对象先于onload脚本执行。所以onload函数应该写在body中而非head -->
<body onLoad="window.alert('页面内容完成导入到浏览器')">
    <input type="button" name="button" title="新建申请表单" value="新建" class="btn" onmouseover="this.className='overstyle'" onmouseout="this.className='outstyle'" onclick="javascript:submit();">

    <!-- // js放在body主体之间用来实现某些部分动态地创建文档 -->
    <script>
        

    </script>
</body>
</html>
```



