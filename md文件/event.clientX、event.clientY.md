**event.clientX、event.clientY**

鼠标相对于浏览器窗口可视区域的X，Y坐标（窗口坐标），可视区域不包括工具栏和滚动条。IE事件和标准事件都定义了这2个属性

**event.pageX、event.pageY**

类似于event.clientX、event.clientY，但它们使用的是文档坐标而非窗口坐标。这2个属性不是标准属性，但得到了广泛支持。IE事件中没有这2个属性。

**event.offsetX、event.offsetY**

鼠标相对于事件源元素（srcElement）的X,Y坐标，只有IE事件有这2个属性，标准事件没有对应的属性。

**event.screenX、event.screenY**

鼠标相对于用户显示器屏幕左上角的X,Y坐标。标准事件和IE事件都定义了这2个属性

offsetWidth       //返回元素的宽度（包括元素宽度、内边距和边框，不包括外边距）

offsetHeight      //返回元素的高度（包括元素高度、内边距和边框，不包括外边距）

clientWidth        //返回元素的宽度（包括元素宽度、内边距，不包括边框和外边距）

clientHeight       //返回元素的高度（包括元素高度、内边距，不包括边框和外边距）

style.width         //返回元素的宽度（包括元素宽度，不包括内边距、边框和外边距）

style.height       //返回元素的高度（包括元素高度，不包括内边距、边框和外边距）

scrollWidth       //返回元素的宽度（包括元素宽度、内边距和溢出尺寸，不包括边框和外边距），无溢出的情况，与clientWidth相同

scrollHeigh       //返回元素的高度（包括元素高度、内边距和溢出尺寸，不包括边框和外边距），无溢出的情况，与clientHeight相同