运行vue时报错  https://blog.csdn.net/Umbrella_Um/article/details/98627947

# You are using the runtime-only build of Vue where the template compiler is not available.Either pre-compile the templates into render functions, or use the compiler-included build.报错处理

只运行，不能编译template   

与package.json同级别的位置自己创建一个vue.config.js文件，

添加module.exports = {
runtimeCompiler: true
}

重启项目  解决  



# [Vue warn]: Avoid using non-primitive value as key, use string/number value instead.

避免使用非基元值作为键，而是使用字符串/数字值。

```
<div v-for="item in workData" :key="item"><div>
```

item是数组或者对象

改为：

```
<div v-for="(item,index) in workData" :key="index"></div>
```





# 报错： [Vue warn]: Invalid prop: type check failed for prop "data". Expected Array, got String.

1.报错：data是一个Array值，可是，获得的却是一个String.![1.报错：data是一个Array值，可是，获得的却是一个String.](https://img-blog.csdn.net/20180927180458688?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xneDE5OTQwNTA4/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

解决：将data的字符串形式改成 v-bind:data=”data1”(相当于:data=”data1”)
![在这里插入图片描述](https://img-blog.csdn.net/20180927180519700?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xneDE5OTQwNTA4/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

