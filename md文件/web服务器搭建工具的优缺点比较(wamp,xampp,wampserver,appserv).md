**WAMP**是指在Windows服务器上使用Apache、MySQL和PHP的集成安装环境，可以快速安装配置Web服务器，一般说来，大家都习惯 于将Apache、MySQL、PHP架设在Linux系统下，但是，不可否认的是Windows也有其优点，就是易用，界面友好，软件丰富，操作起来非常方面，因此对新手来说在Windows平台下使用Apache、MySQL、PHP也确实是一个不错的选择。下面我将介绍几款在Windows下可以使用的WAMP集成环境。

**XAMPP** - XAMPP是一款具有中文说明的功能全面的集成环境，XAMPP并不仅仅针对Windows，而是一个适用于Linux、Windows、Mac OS X 和Solaris 的易于安装的Apache 发行版。软件包中包含Apache 服务器、MySQL、SQLite、PHP、Perl、FileZilla FTP Server、Tomcat等等。默认安装开放了所有功能，安全性有问题，需要进行额外的安全设定。

**WampServer** - WampServe集成了Apache、MySQL、PHP、phpmyadmin，支持Apache的mod_rewrite，PHP扩展、Apache模块只需要在菜单“开启/关闭”上点点就搞定，省去了修改配置文件的麻烦。

**AppServ** - 集成了Apache、PHP、MySQL、phpMyAdmin，较为轻量。

总的来说，无论从安全性和性能上来讲，LAMP（Linux + Apache + MySQL + PHP）都优于WAMP（Windows + Apache + MySQL + PHP），不过由于Windows具有易用的特点，WAMP也未尝不是初学者的一个不错的选择。



[链接：https://pan.baidu.com/s/1CeKYJrD31qZTxODnwOYvwg 密码：jdc0]  wampserver64位下载链接



### wampserver启动后一直呈呈现橙色：解决方法如下

计算机管理--->服务--->SQL Server Reposting Server 右键点击停止---->重启wampserver          										     找不到SQL Server Reposting Server服务  未解决

端口问题：计算机管理---->World Wide Web Publishing  双击  将启动改为禁用  ---->重启wampserver         （win10     				  会自动开启万维网服务，万维网服务会占用80端口，即80端口会默认被IIS(windows自带的服务器软件)占用，				  wampserver也是默认使用80端口，所以启动wampserver时会造成端口冲突，所以禁用万维网服务即可。打开IIS信息服务管理器，右键停止服务   未解决  80端口仍然被占用   /停止IIs服务   

一直是橙色，可能服务没有完全开启，可能apache服务器没有启动。左键wampserver图标--->apache--->服务---->测试80端口， 如下提示：Your port 80 is actually used by :Information not available (might be Skype).Cannot install the Apache service, please stop this application and try again.Press Enter to exit...  说明80端口被占用。关闭占用80端口的程序或者改变apache服务器的端口号。找不到万维网服务World Wide Web Publishing，所以去**改变apache服务器端口号：**输入一个可以用的端口即可，但是使用默认的80端口方便很多。找到apache的配置文件httpd.conf，找到
       #Listen 12.34.56.78:80
       Listen 80
      把 所有 80改成  8081就可以了（当然也可以改成其他端口）如果还是不行的话，可能是你以前安装过，没有卸载干净，强力卸载后在重装就可以了。

到wamp安装目录下的wampmanager.tpl文件，记事本打开：在localhost后面添加8081端口，保存，退出并重新打开wamp  未解决

https://jingyan.baidu.com/article/ac6a9a5e36977d2b653eacb2.html  开启IIS信息服务管理器



端口号一直被占用 ，尝试过禁用iis服务等，80端口依然被占用；修改wampserver端口号，重启后依然是橙色，apache依然无法启动。在服务里手动启动wampapache ,服务报错：Windows不能在本地计算机启动wampapache，有关更多信息，查阅系统事件日志，错误代码1

cmd  netstat -ano

https://blog.csdn.net/haoaiqian/article/details/58147079  wampserver无法启动apache 变橙色问题方法汇总



## wampserver安装配置步骤：

https://jingyan.baidu.com/article/c74d6000bb70110f6a595d8d.html  关于配置文件有很大帮助

https://blog.csdn.net/qq_37419077/article/details/90440539

没有更改安装wampserver默认存放网页文件夹的路径【左键wamp图标，点击www目录，打开安装wamp默认存放网页的文件夹，找到安装目录，找到script文件夹，记事本打开config.inc.php，找到"$wwwDir = $c_installDir.'/www' ;"  ,改成自己想要的目录。比如："$wwwDir = $D:/website'; "】-----该操作只是修改www目录这个链接。打开httpd.conf，找到DocumentRoot 修改后面的值，改成我们网站存放的实际地址就ok；没有修改root的密码，用户root，密码空，进入phpMyadmin；

新增了一个用户hcfa  密码admin

wampserver默认是只允许127.0.0.1访问的，也就是只允许本机访问

apache ----httpd.conf    未找到deny from all  也没有找到allow from 127.0.0.1 ，也没找到allow from all ;如果使用了url重写功能，需要修改allowoverride none--->allowoverride all    这个修改会降低apache的安全性，没有修改；也没有开启url重写功能------找到#LoadMoudule rewrite_moudule modules/mod_rewrite.so，把#去掉，重启apache服务。(如果开启了url重写功能，就一定要修改allowoverride none)；

php配置：找到php.ini 找到short_open_tag = Off  改成On  ---已修改；   找到upload_max_filesize = 2M(上传附件的最大值)   ---------看情况自己改，此处未作修改；    找到memory_limit = 128M（最大使用内存的大小   -----------看清空自己改，未作修改

https://www.jianshu.com/p/9d3b66a8da62

https://www.cnblogs.com/xiaoyaoxingchen/p/7908157.html   配置注意事项







《ThinkPHP 模板 Volist 标签嵌套循环输出多维数组》

U('操作名','array()参数','伪静态后缀名',是否跳转,域名)  thinkphp中的单字母函数  U();  该函数时输出地址

thinkphp  从数据库读取出来的html被直接输出，浏览器不解析；thinkphp写的html代码在浏览器上显示不出来？？？？？

https://wenku.baidu.com/view/d1cc7b1650e79b89680203d8ce2f0066f433641b.html    thinkphp模板文件知识

https://www.jb51.net/article/72089.htm   thinkphp字母函数



### 需要将ThinkPHP框架放入WEB运行环境【前提时WEB运行环境已经OK】：https://blog.csdn.net/qq_35713752/article/details/86631748



php打开报错，未能打开：

( ! ) Warning: include(C:\xampp\htdocs\hcfa\ThinkPHP\Library/Think/Log.class.php): failed to open stream: No such file or directory in D:\wamp\www\hcfa\ThinkPHP\Library\Think\Think.class.php on line *151*    路径问题，没有此类文件或类目录

清除runtime下的缓存，特别要删除common文件夹下的runtime.php

php指定的根目录和源文件不在同一个文件夹

```php
echo $_SERVER['DOCUMENT_ROOT']  //获取PHP项目的根目录路径
```



https://www.cnblogs.com/liwxmyself/p/10249171.html        浏览器滚动事件

https://blog.csdn.net/xiaoxiaoshuai__/article/details/78033514    scrollheight  offsetwidth  scrollwidht  offsetheight clientheight   

offsetTop  获取元素顶边到浏览器可视部分的顶部距离  $(element).offset().top

scrollTop获取元素顶部卷入的距离



https://www.cnblogs.com/mingm/p/6793645.html     边框渐变



