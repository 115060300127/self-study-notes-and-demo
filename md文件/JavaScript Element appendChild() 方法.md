# JavaScript Element appendChild() 方法

**appendChild()**方法将节点附加为节点的最后一个子节点。

```javascript
var node = document.createElement("LI");            //创建<li>节点
var textnode = document.createTextNode("Water");         //创建一个文本节点
node.appendChild(textnode);                              //将文本附加到<li>里面
document.getElementById("myList").appendChild(node);     //将<li>添加到<ul>，id =“myList”里面

```



**提示**：使用 insertBefore()方法在指定的现有子节点之前插入新的子节点。

