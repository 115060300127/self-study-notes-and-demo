# Android的animation由四种类型组成：alpha、scale、translate、rotate，

https://blog.csdn.net/jhope/article/details/78982136 

#### **1、XML配置文件中**

| alpha     | 渐变透明度动画效果       |
| --------- | ------------------------ |
| scale     | 渐变尺寸伸缩动画效果     |
| translate | 画面转换位置移动动画效果 |
| rotate    | 画面转移旋转动画效果     |