## document.createElement("div")     



```javascript
var div = document.createElement("div");
div.innerText = "abc";
效果就是
//<div>abc</div>
```

## document.createTextNode()创建一个文本节点

```javascript
var div = document.createElement("div");
div.appendChild(document.createTextNode("你想要的文本内容"));//document.createTextNode()创建一个文本节点
//<div>你想要的文本内容</div>
```



```javascript
var style = document.createElement("style");
style.type="text/css";
```

