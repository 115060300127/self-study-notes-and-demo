# ES6规范汇总：

### let 命令：用来声明变量。同样是声明变量，`let`与之前的`var`主要有以下几点区别：

1. `let` 声明的变量不允许重复声明:

```jsx
  var a = 1;
  var a = 2;
  console.log(a); //输出 2

  let b = 1;
  let b = 2;     //报错
  console.log(b) ;
```

  2.let` 声明的变量存在块级作用域: 两个代码块中拥有各自的变量`n，不会相互影响。

```jsx
  function f1() {
    let n = 5;
    if (true) {
      let n = 10;
      console.log(n); //输出 10
    }
    console.log(n); // 输出 5
  }
```

再给一个例子：

```ruby
  ///////// 使用 var /////////
  var printNumTwo;
  for (var i = 0; i < 3; i++) {
    if(i === 2){
      printNumTwo = function() {
        return i;
      };
    }
  }
  console.log(printNumTwo());
  // returns 3
  
  ///////// 使用 let /////////
  'use strict';
  let printNumTwo;
  for (let i = 0; i < 3; i++) {
    if (i === 2) {
      printNumTwo = function() {
        return i;
      };
    }
  }
  console.log(printNumTwo());
  // returns 2
  console.log(i);
  // returns "i is not defined"
```

  3.let声明，不存在变量提升

变量提升：当使用`var`声明变量时，变量可以放在声明之前使用，只不过使用还没有声明的变量时，其值为`undefined`。但是使用`let`声明变量时，如果使用放在声明之前则会报错。

```jsx
  console.log(a); //输出undefined   var 变量可以在声明之前使用 只不过值是undefined
  var a = 1;

  console.log(b); //报错 let声明的变量不可以在申明之前使用
  let b = 1;     
```

### const 命令

`const`除了拥有`let`的所有优秀特性(不允许重复声明，有用块级作用域)之外，还有一个特性，那就是它是只读的。

------

*“ const 实际上保证的，并不是变量的值不得改动，而是变量指向的那个内存地址不得改动。对于简单类型的数据（数值、字符串、布尔值），值就保存在变量指向的那个内存地址，因此等同于常量。但对于复合类型的数据（主要是对象和数组），变量指向的内存地址，保存的只是一个指针， const 只能保证这个指针是固定的，至于它指向的数据结构是不是可变的，就完全不能控制了。”*

*《ES6标准入门》 阮一峰*

------

阮大佬上面这段话具体是什么意思呢，请看下面这个例子：

```jsx
  "use strict";
  const s = [5, 6, 7];
  s = [1, 2, 3]; // 报错
  s[2] = 45; // 正常工作
  console.log(s); // returns [5, 6, 45]
```

我们将一个新数组`[1, 2, 3]`赋值给变量`s`时，其实是企图改变变量`s`内指针的指向，而因为变量`s`是使用`const`命令声明的，其指针指向是固定的，不能变动的，所以程序报错。第二次，我们想修改数组中第二项的值，因为修改数组中某一项的值并不改变数组的地址，变量`s`内所保存的指针依旧指向原来的地址，所以程序可以成功执行。

### Object.freeze

因为`const`并不能保证复合类型的数据是不可改变的，所以我们需要一个新的命令来保证复合类型的只读性，这个命令就是`Object.freeze`，具体用法请看下例：

```jsx
  let person = {
    name:"XiaoMing",
    review:"yes"
  };
  Object.freeze(person);
  person.review = "no"; //这条命令将会被忽略，因为此时person对象是只读的
  person.newProp = "age"; // 这条命令也将会被忽略
  console.log(obj); 
  // { name: "XiaoMing", review:"yes"}
```

### 箭头函数 (Arrow Functions)

`ES6` 为我们带来了一种更简洁的书写函数的方式，箭头函数。在旧的标准中，我们是这样书写函数的：

```jsx
  const myFunc = function() {
    const myVar = "value";
    return myVar;
  }
```

使用剪头函数，我们可以将其简化为：

```jsx
  const myFunc = () => {
    const myVar = "value";
    return myVar;
  }
```

还可以进一步简化，我们可以甚至连`return`都不要:

```jsx
  const myFunc = () => "value"
```

箭头函数可以传参:

```jsx
  // 将输入的数乘以2，并返回
  const doubler = (item) => item * 2;
```

箭头函数可以极大的化简高阶函数的使用。所谓高阶函数，就是那些接受一个函数作为参数的函数，常见的有：`map()`，`filter()`，`reduce()`。在以前我们是这么写高阶函数的:

```jsx
  FBPosts.filter(function(post) {
    return post.thumbnail !== null && post.shares > 100 && post.likes > 500;
  })
```

利用箭头函数可以化简为一行：

```jsx
FBPosts.filter((post) => post.thumbnail !== null && post.shares > 100 && post.likes > 500)
```

### 为函数的参数设置默认值

在`ES6`中，你可以为函数的参数设置默认值：

```jsx
  function greeting(name = "Anonymous") {
    return "Hello " + name;
  }
  console.log(greeting("John")); // 输出 Hello John
  console.log(greeting()); // 输出 Hello Anonymous
```

参数`name`的默认值被设置为`"Anonymous"`，所以在不传参直接调用`greeting()`时，输出的是`Hello Anonymous`。

### rest 参数 (Rest Operator)

`ES6`中引入了`rest`参数，其形式为`...变量名`。通过使用`rest`参数，你可以向函数传入不同数量的参数：

```jsx
  function howMany(...args) {
    return "You have passed " + args.length + " arguments.";
  }
  console.log(howMany(0, 1, 2)); // 传入三个参数
  console.log(howMany("string", null, [1, 2, 3], { })); // 传入四个参数
```

`rest`参数中的变量代表一个数组，所有数组特有的方法都可以用于这个变量：

```jsx
  function push(array, ...items) {
    items.forEach(function(item) {
      array.push(item);
      console.log(item);
    });
  }
  var a = [];
  push(a, 1, 2, 3)
  console.log(a)  //输出 [1， 2， 3]
```

`rest`参数之后不能再有其他参数，否则程序会报错。

### 扩展运算符 (Spread Operator)

扩展运算符其实就是`rest`参数中的那三个点`...`，其作用是将数组打散：

```jsx
  console.log(...[1, 2, 3])
  // 1 2 3
  console.log(1, ...[2, 3, 4], 5)
  // 1 2 3 4 5
  [...document.querySelectorAll('div')]
  // [<div>, <div>, <div>]
```

在之前，如果我们想求数组中的最大值需要这样写：

```jsx
  var arr = [6, 89, 3, 45];
  var maximus = Math.max.apply(null, arr); // 返回 89
```

因为`Math.max()`方法只接受由逗号分隔的参数序列，比如`Math.max(1, 2, 3)`，所以我们需要`apply()`进行转化。在`ES6`中，我们不再需要`apply()`方法，而是可以直接利用扩展运算符，其形式更易于理解：

```jsx
  const arr = [6, 89, 3, 45];
  const maximus = Math.max(...arr); // 返回 89
```

**请注意**，扩展运算符`...`只在某些特定的情况下才可以使用，比如函数的参数中，或者数组中。裸用扩展运算符程序会报错:

```csharp
  var arr = [6, 89, 3, 45]
  const spreaded = ...arr; //报错
```

### 解构赋值(Destructuring Assignment)

我们已经看过了扩展运算符可以让我们的数组操作变得多么高效。对于操作对象，`ES6`也给出了相似的方法。

1. 请看下面这个`ES5`的例子:

```csharp
  var voxel = {x: 3.6, y: 7.4, z: 6.54 };
  var x = voxel.x; // x = 3.6
  var y = voxel.y; // y = 7.4
  var z = voxel.z; // z = 6.54
```

在`ES6`中，我们可以这样做：

```cpp
  const { x, y, z } = voxel; // x = 3.6, y = 7.4, z = 6.54
```

如果你想赋值的变量与对象的属性有不同的名称，你可以这样做:

```cpp
  const { x : a, y : b, z : c } = voxel // a = 3.6, b = 7.4, c = 6.54
```

以上操作方法被我们称为解构赋值。

1. 解构赋值也可以作用于嵌套的对象:

```cpp
  const a = {
    start: { x: 5, y: 6},
    end: { x: 6, y: -9 }
  };
  const { start : { x: startX, y: startY }} = a;
  console.log(startX, startY); // 5, 6
```

1. 利用解构赋值，我们可以轻易的获取数组的特定元素：

```cpp
  const [a, b] = [1, 2, 3, 4, 5, 6];
  console.log(a, b); // 1, 2

  const [a, b,,, c] = [1, 2, 3, 4, 5, 6];
  console.log(a, b, c); // 1, 2, 5
```

1. 解构赋值可以搭配扩展运算符使用

```cpp
  const [a, b, ...arr] = [1, 2, 3, 4, 5, 7];
  console.log(a, b); // 1, 2
  console.log(arr); // [3, 4, 5, 7]
```

1. 如果你想将一个对象当参数传入，你可能会想到这样做：

```jsx
  const profileUpdate = (profileData) => {
    const { name, age, nationality, location } = profileData;
    // 函数操作
  }
```

但其实你可以这样：

```jsx
  const profileUpdate = ({ name, age, nationality, location }) => {
    // 函数操作
  }
```

这样做的好处是，我们不用把整个对象都传进来，只需要传入我们需要的那部分。

### 模板字符串 (Template String)

`ES6`中引入了一种更强大的字符串写法，被称为模板字符串，用反引号( ` )标识，用法如下：

```jsx
  const person = {
    name: "Zodiac Hasbro",
    age: 56
  };

  //用模板字符串方式书写的字符串，并将其赋给greeting变量
  const greeting = `Hello, my name is ${person.name}!
  I am ${person.age} years old.`;

  console.log(greeting); 
  // 输出：
  // Hello, my name is Zodiac Hasbro!
  // I am 56 years old.
```

在上面这段代码中，有三个地方需要我们注意：

1. 模板字符串的标识符是反引号`(`)` 而不是单引号`(')`
2. 输出的字符串是多行的，我们在也不需要`\n`了
3. 语法`${}`可以用来获取变量，化简了之前用`+`来进行字符串拼接的写法

### 更简洁的定义对象的方法

在`ES5`中我们是这样定义对象的方法的:

```jsx
  const person = {
    name: "Taylor",
    sayHello: function() {
      return `Hello! My name is ${this.name}.`;
    }
  };
```

在`ES6`中，我们可以将`function`关键词省略：

```jsx
  const person = {
    name: "Taylor",
    sayHello() {
      return `Hello! My name is ${this.name}.`;
    }
  };
```

### class

`ES6`中提供了一种新的语法创建对象，即使用`class`关键词。需要注意的是，这里的`class`关键词只是语法糖，并不具有像传统的面向对象的语言那样的特性。

在`ES5`中，我们通常是这样创建构造函数的:

```jsx
  var Person = function(name){
    this.name = name;
  }
  var person1 = new Person('Jim');
```

利用`class`语法糖，我们可以这样写:

```jsx
  class Person {
    constructor(name){
      this.name = name;
    }
  }
  const person1 = new Person('Jim');
```

在由`class`定义的对象中，我们添加了构造函数`constructor()`，构造函数在`new`被调用时唤醒，创建一个新的对象。

### 用取值函数和存值函数(getters and setters)来封装对象

在由`class`定义的对象中，存值函数和取值函数现在有了自己的关键字`get`和`set`，用法也更加简单:

```jsx
  class Book {
    constructor(author) {
      this._author = author;
    }
    // getter
    get writer(){
      return this._author;
    }
    // setter
    set writer(updatedAuthor){
      this._author = updatedAuthor;
    }
  }
  const lol = new Book('anonymous');
  console.log(lol.writer);  // anonymous
  lol.writer = 'wut';
  console.log(lol.writer);  // wut
```

请注意我们调用存值函数和取值函数的方式：`lol.writer`。这种调用方式让他们看起来并不像是函数。