## jQuery   end()

end()方法用于在当前操作结束后，将匹配到的元素集还原到之前的状态。

```javascript
$('div').find('p').css('color','red').end().css('background','green');
//表示先找到div标签内的p标签设置字体颜色，当字体颜色设置完成之后，还原到之前的状态，即此时的对象依然时一开始的div标签，而不是p标签。将div元素的背景设置为绿色
```



###  toggleClass('类名') 如果存在该类名就删除它，如果不存在该类名就添加它



## 对浏览器窗口调整大小进行计数：

```javascript
//当调整浏览器窗口的大小时，发生 resize 事件。
$(window).resize(function() {
  $('span').text(x+=1);
});
```





$(window).width()  获取浏览器宽度

window.innerWidth能获取当前窗口的宽度(包含滚动条)，