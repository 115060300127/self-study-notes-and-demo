svg transform表示什么意思？

https://www.tuicool.com/articles/VfyMfua  transform详细介绍

```html
<!DOCTYPE html>
<html>
<body>

<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <text x="0" y="15" fill="red" transform="rotate(30 20,90)">I love SVG</text>
</svg>
  <!--使文本绕点（20，90）旋转30°角-->
</body>
</html>
```



```
将svg元素倾斜   
skewX(<skew-angle>)
skewY(<skew-angle>)
```



如果你想要一个元素围绕它的中心旋转，你也许想要像CSS中一样声明中心为 `50% 50%`；不幸的是，在 `rotate()` 函数中这样做是不允许的-你必须用绝对坐标。然而，你可以在CSS的 `transform` 属性中使用 `transform-origin` 属性。