Table 4: 一些 Target 属性值
值 简要描述
_blank 在新的窗口中打开该链接
_parent 在本窗口的父窗口中打开该链接
_self 在本窗口中打开该链接
_top 在根窗口中打开该链接