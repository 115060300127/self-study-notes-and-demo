# <u>h5</u><u>新特性</u>：

- ## 一，本地存储：数据持久化---将数据“长久地”保存在客户端的操作  【比如：登录页面后保存登录数据，刷新页面后数据仍然存在，一段时间内都保存数据，不用重复登录】

  ####   1 . cookie  :可以认为是一个记录了简单内容的文本文件，直接绑定在html页面上。不常用，难懂

  --------------------cookie的设置分为前端设置和后端设置。

  ---- - ------**前端操作cookie-**--------------------------------：

     1.写入cookie ：document.cookie = ' key=value;expires=过期时间戳 ' **;**

  ​	  【！！前端操作cookie时，时间戳**必须**通过**toGMTString()**方法转换为标准时间才行】

  ​	2.读取cookie：console.log(document.cookie)**;**

  ​	3.删除cookie：document.cookie = 'key=value;expires=当前时间戳+1'**;**

  ​    **单位是毫秒**

  ```javascript
  var date = new Date();//获取时间戳
  console.log(date.getTime());//getTime()获取时间戳的毫秒数 number
  var expiresss = new Date(date.getTime()+1000*1000).toGMTString();//设置过期时间戳
  console.log(expires);
  //写入cookie
  document.cookie = 'username=frank;expires='+expiresss;
  //如果不删除cookie,把写入和读取cookie的代码删除后再刷新页面，cookie仍然存在，只有时间戳到期后或者重新设置代码删除cookie，cookie才会被删除
  
  var tempArr = document.cookie.split('=');
  var tempObj = {};
  tempObj[tempArr[0]] = tempArr[1];
  console.log(tempObj);
  console.log(tempObj.username);
  
  //删除cookie
  var expiresss = new Date(date.getTime()+1).toGMTString();
  ```

   --------------------**后端操作cookie**----------------------------------------------

   1.写入cookie ：setcookie('key','value','expires','path')**;**

    2.获取cookie：$_COOKIE**;**

    3.删除cookie：setcookie('key','value','expires+1','path')**;**  

  ​                           setcookie('key','value','expires-1','path')**;**  

    [说明： 哪个html页面访问了本php文件，就给哪个html页面添加cookie，在php中获取时间戳的方式为time()，**单位是s；** 前三个参数为必要参数，path为可选参数]

  ```javascript
  //<button>点我添加cookie</button>
  //----------------------------------------------
  <script>
      	(function() {
      		var cookie = document.cookie;
      		if(cookie.length != 0) {
                  cookie.log('已经登录，不用再显示登录按钮');
              }
  		})();
      
  		document.querySelector('button').onclick = function() {
      			//Ajax给后端发送请求
  			    var xhr = new XMLHttpRequest();//创建对象
      			xhr.onreadystatechange = function() {
                      if(xhr.readyState==4 && xhr.status==200) {
                          console.log(xhr.responseText);//打印后端的返回值
                      }
                   };
      			var formData = new FormData();//FormData()用于将数据编译成键值对，以便用XMLHttpRequest来发送数据----主要用于发送表单数据，也可用于发送带键数据
      			formData.append('uname','frank');//添加字段
                  //formDate对象的字段类型可以时Blob,File,string  如果字段类型不是Blob,也不是File,就会被转成string
      
      			xhr.open('post','lesson2_cookie(server).php',true);
      		    xhr.send(formData);	
  		}
  </script>
  ```

  

  ```php
  <?php
      $username = $_POST['uname'];
  	$success = array('msg'=>'ok','info'=>$_POST);
  
  	//设置cookie
  	setcookie('uname',$username,time()+60*60*24);//60s*60-->表示1h，60*60*24-->表示一天
  	//读取cookie    echo json_encode($_COOKIE);
  	//删除cookie    setcookie('uname','',time()-1);
  
  	setcookie('key','value','expires','path');//1.写入cookie
  //path指的是cookie要加载到当前域名的哪个位置 设置cookie生效的范围【只有cookie有path】
    	$_COOKIE;//2.获取cookie
    	setcookie('key','value','expires+1','path');//  3.删除cookie
  	setcookie('key','value','expires-1','path');//  3.删除cookie
  
  	//给出反馈
  	$success = array('msg'=>'OK');
  	echo json_encode($success);
  
  ?>
  ```

  

  2. #### localStorage ： 持久保护客户端数据，用户不删除就会一直/永远存在  【在任何位置都能随便访问

     ```html5
     localStorage.setItem('key','value')//存储
     localStorage.getItem('key')//读取
     localStorage.remove('key')//删除
     ```

  3. #### sessionStorage ： 数据一旦被本地化后，只要页面不关闭就不会主动删除，一旦浏览器关闭后，数据就被自动删除  【在任何位置都能随便访问

     ```
     sessionStorage.setItem('key','value')//存储
     sessionStorage.getItem('key')//读取
     sessionStorage.remove('key')//删除
     ```



- ## 二，Webworker：其他方式实现的异步执行，ajax，异步加载 使得web内容能够在后台运行脚本   【！！！所有异步操作一定有一个回调函数来表示：当异步完成后要干什么】

  ​				

  ```javascript
  <button>点我开启webWorker</button>
  <script>
  var num = 0;
  var timer = null;
  timer = setInterval(function() {
  	console.log(num++);
  },1000);
  
  //开启webWorker,让指定的js脚本后台异步执行
  document.querySelector('button').onclick = function() {
  	//生成worker
  	var myworker = new Worker('js路径');
      //事件处理函数 myworker.onmessage = function(result) {}
  	myworker.onmessage = function(data){
  		console.log(eve.data);
  	};
      //传递数据     postMessage('data')   写在引入的js文件中
  };
  
  </script>
  ```

  

- ## 三，File API:   上传文件 【 选中文件后，对文件进行一个预加载】

```
FileReader:

描述：FileReader是html5新增的一个类；用来对input中【文件上传操作file类型】进行处理的类；
	 通过该类创建的对象中提供一些方法可以对上传的文件进行【预先的读写】，从而在某种情况下实现预览。

说明：文件读写一般分为三种方式：
	1.文本读取
	2.图片url读取
	3.二进制流读取
	
语法：
```



```javascript
var fileReader = new FileReader();
//获取文本的格式
fileReader.readAsText(从input元素中获得的文件流);
fileReader.onload = function() {
	console.log(fileReader.result);	
};
//读取图片url

```



 

```javascript
-------------html-------------
<input type='file' class='file' multiple>
<button>上传文件</button>
<div class="showDiv"></div>
<img src="">
    
------------js---------------

var fileInput = document.querySelector('.file');
var btn = document.querySelector('button');
var showDiv = document.querySelector('.showDiv');
var img = document.querySelector('img');
//
btn.onclick = function() {
	var fileReader = new FileReader();
    fileReader.readAsText(fileInput);
}
```





- ## 四，getUserMedia： 获取用户的音频视频设备，比如摄像头，麦克风