# vue中$refs的用法：与ref的区别

   1.ref加在普通的元素上，用this.$refs.ref值   获取到的是dom元素；

2. ref加在子组件上，用this.$refs.ref值  获取到的是组件实例，可以使用组件的所有方法。在使用方法的时候直接this.$refs.ref值.方法()       就可以使用了；
3. 利用v-for和ref获取一组数组或者dom节点。 https://blog.csdn.net/wh710107079/article/details/88243638

ref需要在dom渲染完成后才会有，可以在mounted方法中，或者this.$nextTick()中调用