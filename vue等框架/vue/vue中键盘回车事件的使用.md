vue中键盘回车事件的使用

https://www.cnblogs.com/cristina-guan/p/9440035.html



**【Vue】var that=this写法的意义**  

this代表父函数，that只是表示一个变量名，如果在子函数中还是用this，this.goerp()中this的指向就变成了子函数，在子函数中找不到goerp方法

```js
var that = this; //this代表父函数，that只是表示一个变量名，如果在子函数中还是用this，this.goerp()中this的指向就变成了子函数，在子函数中找不到goerp方法  如果直接使用this.goerp()  会显示找不到goerp这个方法 ，因为在function(e)里面没有这个方法
        document.onkeydown = function (e) {
            var key = window.event.keyCode;
            if(key == 13) {
                that.goerp();
            }
        }
```

https://blog.csdn.net/heni6560/article/details/91570048



### @click.native的作用：

如果使用router-link标签，加上@click事件，绑定的事件会无效因为：router-link的作用是单纯的路由跳转，会阻止click事件，你可以试试只用click不用native,事件是不会触发的。此时加上.native，才会触发事件。

