# vue 上拉加载/下拉刷新数据的插件的使用：

1.安装：

​		npm i vue-scroller -S

​		npm install vue-scroller -D

2.在需要加载的页面中引入，或在公共js文件中引入：

​		import VueScroller from 'vue-scroller'

​		Vue.use(VueScroller)

3.在模板中使用：

​	<scroller :on-refresh="refresh"  :on-infinite="infinite" > </scroller>

​	:on-refresh:下拉刷新                   :on-infinite:上拉加载

​	另一种在模板中的用法：

```html
<!-- 下拉的代码段：使用VuePullRefresh标签嵌套 :on-refresh="onRefresh"表示下拉时要执行的方法
	method:{onRefresh:function(){}} -->
        <VuePullRefresh :on-refresh="onRefresh">
          <!-- 使用v-for更新已渲染过的元素列表 为防止v-for报错， 一定要v-bind绑定key key是唯一的，指向列表中每个元素的唯一值 -->
          <div class="info url log" v-for="(item,index) in moreListData" :key="index">
              <div class="poster">
                  <img :src="item.pic_big" :alt="item.title">
              </div>
              <div class="text-wrap">
                  <div class="title">{{ item.title }}</div>
                  <div class="author">{{ item.artist_name }}</div>
              </div>
          </div>
        </VuePullRefresh>
```

