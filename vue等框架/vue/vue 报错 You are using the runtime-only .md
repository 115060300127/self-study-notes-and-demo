# vue 报错 You are using the runtime-only build of Vue where the template compiler is not available.



你正在使用’仅有运行时环境’的Vue构建版本，该版本中template编译器不可用。要么预编译模板，使其变为render函数，或者使用’包含编译器’的Vue构建版本。



https://blog.csdn.net/C_ZhangSir/article/details/100989902   解决方法



方法一：将`main.js`中的`import Vue from 'vue'`更改为`import Vue from 'vue/dist/vue.js'`得到解决

解决的原因：'vue/dist/vue.js'版本有compiler



方法2：

1. 在项目根目录创建vue.config.js文件，编写如下代码

```
module.exports = {
  runtimeCompiler: true
}
123
```

1. 重新运行项目

