# vue中created和mounted等方法整理：

**钩子函数**是Windows消息处理机制的一部分，通过设置“钩子”，应用程序可以在系统级对所有消息、事件进行过滤，访问在正常情况下无法访问的消息。钩子的本质是一段用以处理系统消息的程序，通过系统调用，把它挂入系统。对于前端来说，钩子函数就是指**在所有函数执行前，先去执行了的函数。**

https://segmentfault.com/a/1190000008010666

1 created：页面加载之前执行，在实例创建完成后被立即调用。执行顺序：父组件-子组件 可以执行多次  因为执行	   		created钩子函数的时候，页面还没有渲染出来，所以如果在此操作dom元素，一定会找不到相关元素，会报错。    		在created 接受数据，然后$nextTick(function(){做swiper初始化})，    vue请求初始化数据放在这里

2 mounted：页面加载完成后执行。执行顺序：子组件-父组件   在mounted里面的都是钩子函数 可以直接操作dom节点   		生命周期--钩子  页面初始化  mounted在整个vue实例的生命周期中只执行一次！！在页面第一次加载完成后才会被  		调用出来。在mounted中发起后端请求，拿回数据，配合路由钩子做一些事情。

3 methods：事件方法执行

4 watch：watch是去监听一个值的变化，然后执行相对应的函数。

5 computed：computed是计算属性，也就是依赖其它的属性计算所得出最后的值

## 生命周期：

**beforeCreate**     实例被完全创建出来之前执行，数据和方法都还没有被初始化。数据观测和event/watcher时间配置之前被调用

**created**     created的时候数据和方法都初始化完成，data和methods可以调用了。如果要调用方法或者操作data中的数据，最早只能在created中

 **beforeMount**    表示模板已经编译完成放在内存中，但还没有把模板挂载到页面中  【挂载之前，dom的初始化只是虚拟dom，先把坑站住，mounted挂载才会把值渲染进去

**mounted** 			将内存中编译好的模板真实地替换到浏览器的页面中去  挂载完成   此时页面已经被渲染完成，实例被完全创建好

beforeUpdate   组 件更新之前

updated            组件更新之后

activated 			组件被激活时调用

deactivated 		组件被移除时调用

beforeDestroy   监听页面间的跳转   组件销毁前调用

destroyed   			组件销毁后调用

errorCaptured







javascript的加载卸载事件：

onload  页面加载时

页面关闭时，先onbeforeunload  ,再onunload

页面刷新时，先执行 onbeforeunload,  再onunload  ,  最后onload

