vue报错总结：https://blog.csdn.net/weixin_43043994/article/details/82251305

# vue项目报错总结

## 一、space、tab报错

```
Unexpected tab character
Missing space before function parentheses
expected "indent", got "outdent"
123
```

这些报错都是空格和tab的报错问题

**解决方法**：

- 1、因为你设置了eslint，如果你不想有规范的js代码，可以重新初始化关掉eslint。

`Use ESLint to lint your code? (Y/n)` 这一步选no

在**bulid/webpack.base.conf.js**里面有配置如下：

```
module: {
rules: [
...(config.dev.useEslint ? [createLintingRule()] : []),
123
```

发现在config目录下index.js文件中，将`useEslint: true`改为`useEslint: false`

![如图](https://img-blog.csdn.net/20180831091913261?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzA0Mzk5NA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

- 2、在**eslintrc.js**下添加一行`"no-tabs":"off"`

![这里写图片描述](https://img-blog.csdn.net/20180831091954420?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzA0Mzk5NA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

## 二、标签不变色问题

- 1、vue后缀的文件标签不变色

下载一个**vue-syntax-highlight-master**的插件即可，下载完成后复制到**Sublime Text3\Data\Packages**目录下（在sublime中点击 **首选项-浏览插件**即可进入该目录）

- 2、style标签下不变色

若设置了属性**`lang=stylus`**，下载**stylus插件**即可

步骤如下：

**首选项-package control-install package-stylus**

## 三、package controll报错

需要下载**package control**才能在**sublime**安装插件

解决方法就是下载一个新的package control 替换掉原来的就可以了

- 1、在百度搜索package control，点击搜索项第一个，即为package control官网。
- 2、在打开后的页面，点击绿色按钮`“install now”`，跳转到安装页面

一种是下载package control的应用程序，
另一种是simple方法。

这里分为sublime text2 和sublime text3 安装，根据自己的版本选择合适的安装方法。

复制安装命令。

- 4、打开sublime text3 软件，选择 view（视图），命令行，或者按快捷键Ctrl +·(tab键上面那个键)，即可打开命令行。
- 5、在命令行中粘贴步骤3中的复制内容
- 6、回车进行安装。

## 四、template报错

**报错信息：**

```
You are using the runtime-only build of Vue where the template compiler is not available. 
Either pre-compile the templates into render functions, or use the compiler-included build.
12
```

您只使用VUE的运行时版本，其中模板编译器不可用。要么将模板预编译成渲染函数，要么使用编译器包含的构建。

运行时构建不包含模板编译器，因此不支持 template 选项，只能用 render 选项，但即使 使用 运行时构建，在单文件组件中也依然可以写模板，因为单文件组件的模板会在构建时预编译为 render 函数。
**解决办法：**
在webpack.base.conf.js中配置别名

```
resolve: {
    alias: {
      'vue':'vue/dist/vue.common',
    }
  }
12345
```

## 五、lang=“stylus” scoped

**安装stylus：**
运行npm install stylus stylus-loader --save
即可使用

```
<style lang="stylus" scoped>
</style>
12
```

scoped属性，它的存在可以让我们的组件的样式保持独立性
如果使用该属性，则样式仅仅应用到 style 元素的父元素及其子元素
在 XHTML 中, 属性是不允许简写的，scoped 属性必须定义为：`<style scoped="scoped" />`。

### 六，Unknown custom element router-view 未知的自定义元素：<router link>-是否正确注册了组件？对于递归组件，请确保提供“name”选项。

**解决**：在main.js中引入

```
import VueRouter from 'vue-router';

Vue.use(VueRouter);// 使用vue.use函数明确地安装路由
```

### 七：$attrs is readonly. $listeners is readonly

出现这个问题的原因，主要是因为在使用的时候出现了A组件调用B组件，B组件再调用了C组件。而直接使用了A组件修改C组件的数据  //或者是导入了两次vue

这个错误会导致被点击router-link不再自动生成router-link-exact-active和router-link-active两个class，而且原来的router-link的router-link-exact-active和router-link-active两个class也不会自动删除。

**解决方法**：将两次导入的import Vue from 'vue';都改成import Vue from 'vue/dist/vue.js';  依然还是导入了两次vue 但没有报错了



### 八，未预料到的导航重复，{ name：“导航重复”，name:"导航重复“，message:"导航到当前位置是不被允许的”，stack堆栈：“在新导航时出错重复”（网页包-int，，，内部：///）}

 Uncaught (in promise) NavigationDuplicated {_name: "NavigationDuplicated", name: "NavigationDuplicated", message: "Navigating to current location is not allowed", stack: "Error↵    at new NavigationDuplicated (webpack-int…ternal:///./node_modules/vue/dist/vue.js:2187:16)"}

**原因：**按钮点击是切换到base路由,当点击一次已经切换到base路由后，再点击按钮，由于目前已经是在base路由，所以就导致导航重复报错了。

### 九：[Vue warn]: Do not use built-in or reserved HTML elements as component id: nav不要将内置或保留的html元素用作组件id:nav

原因：

```
<script>
export default {
    name: "nav",//name属性值不可以是nav header footer等html元素，否则就报这个错，而且不可以为中文
    data() {
        return {
        }
    },
}
</script>
```

### 十：Uncaught Error: Cannot find module './iconfont.svg?t=1571298404241'  未捕获错误：找不到模块 

原因：没有引入iconfont.svg文件  使用图标需要引入的文件有：.eot  .json  .svg  .ttf  .woff   .woff2