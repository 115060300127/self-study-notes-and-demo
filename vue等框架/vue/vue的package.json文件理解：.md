# vue的package.json文件理解：是node.js的模块

https://www.cnblogs.com/tzyy/p/5193811.html#_h1_0  

https://www.cnblogs.com/hongdiandian/p/8321039.html

当创建一个node项目，意味着创建一个module模块，这个模块的描述文件就是package.json

卸载本地package：npm uninstall <package_name>

```json
{//name和version属性必须要有，否则模块无法被安装。这两个属性一起形成了一个Npm模块的唯一标识符
    
    
  "name": "vuemusic",  //包/模块名称，会成为url的一部分  name不能以_或.开头，不能包含大写字母，不能含有url非法字符
  "version": "1.0.0",  //包的版本号 必须是可以被npm依赖的一个node-semver模块解析
  "description": "A Vue.js project",  //包的描述
  "author": "iwensxt <liuxingyu@sxt.cn>",  //包的作者
  "private": true,  //如果值为true，npm将拒绝发布它
  "scripts": {  //支持的脚本
    "dev": "webpack-dev-server --inline --progress --config build/webpack.dev.conf.js", 
    "start": "npm run dev",
    "build": "node build/build.js"
  },
  "dependencies": { //依赖包 npm install <package_name> --save
      //表示将这个包名及对应的版本添加到package.json的dependencies   配置模块依赖的模块列表
    "axios": "^0.18.0",
    "pull-to-refresh": "^1.2.3",
    "vue": "^2.5.2", //vue版本
    "vue-awesome-swiper": "^3.1.3",
    "vue-pull-refresh": "^0.2.7",
    "vue-router": "^3.0.1"  //路由版本
  },
  "devDependencies": { //生产环境，开发阶段需要 npm install <package_name> --save-dev表示将这个包名及对应的版本添加到package.json的devDependencies  这里写的依赖用于开发环境，不发布到生成环境
    "autoprefixer": "^7.1.2",
    "babel-core": "^6.22.1",
    "babel-helper-vue-jsx-merge-props": "^2.0.3",
    "babel-loader": "^7.1.1",
    "babel-plugin-syntax-jsx": "^6.18.0",
    "babel-plugin-transform-runtime": "^6.22.0",
    "babel-plugin-transform-vue-jsx": "^3.5.0",
    "babel-preset-env": "^1.3.2",
    "babel-preset-stage-2": "^6.22.0",
    "chalk": "^2.0.1",
    "copy-webpack-plugin": "^4.0.1",
    "css-loader": "^0.28.0",
    "extract-text-webpack-plugin": "^3.0.0",
    "file-loader": "^1.1.4",
    "friendly-errors-webpack-plugin": "^1.6.1",
    "html-webpack-plugin": "^2.30.1",
    "node-notifier": "^5.1.2",
    "optimize-css-assets-webpack-plugin": "^3.2.0",
    "ora": "^1.2.0",
    "portfinder": "^1.0.13",
    "postcss-import": "^11.0.0",
    "postcss-loader": "^2.0.8",
    "postcss-url": "^7.2.1",
    "rimraf": "^2.6.0",
    "semver": "^5.3.0",
    "shelljs": "^0.7.6",
    "uglifyjs-webpack-plugin": "^1.1.1",
    "url-loader": "^0.5.8",
    "vue-loader": "^13.3.0",
    "vue-style-loader": "^3.0.1",
    "vue-template-compiler": "^2.5.2",
    "webpack": "^3.6.0",
    "webpack-bundle-analyzer": "^2.9.0",
    "webpack-dev-server": "^2.9.1",
    "webpack-merge": "^4.1.0"
  },
  "engines": { //声明项目需要的node或npm版本范围
    "node": ">= 6.0.0",
    "npm": ">= 3.0.0"
  },
  "browserslist": [ //在不同的前端工具之间共享目标浏览器的库，确定哪些支持哪些版本的浏览器
    "> 1%",  //全球有超过1%的人使用的浏览器
    "last 2 versions",	//根据CanIUse.com追踪的最后两个版本的所有浏览器
    "not ie <= 8"  //排除之前查询选择的浏览器
  ]
}

```

