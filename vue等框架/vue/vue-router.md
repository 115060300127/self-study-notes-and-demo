vue-router

vue单页面开发的路由，决定页面跳转的

```
<router-link :to="‘home‘">Home</router-link>

<router-link :to="{ path: ‘home‘ }">Home</router-link>

//命名路由
<router-link :to="{ name: ‘user‘, params: {userId: 123} }">Home</router-link>

//带查询参数，下面的结果为/register?plan=private-->
<router-link :to="{ path: ‘register‘, query: {plan: ‘private‘}}">Register</router-link>
```



1、to

　　表示目标路由的链接。当被点击后，内部会立刻把to的值传到router-push()。

```
<router-link :to="‘home‘">Home</router-link>

<router-link :to="{ path: ‘home‘ }">Home</router-link>

//命名路由
<router-link :to="{ name: ‘user‘, params: {userId: 123} }">Home</router-link>

//带查询参数，下面的结果为/register?plan=private-->
<router-link :to="{ path: ‘register‘, query: {plan: ‘private‘}}">Register</router-link>
```



2、replace

　　设置replace属性的话，当点击时，会调用roter.replace()而不是router.push()，所以导航后不会留下history记录，也就是不能回退到上一个页面

```
<router-link :to="{path: ‘/abc‘}" replace>ABC</router-link>
```

3、append

　　设置append属性后，则在当前路径前添加基路径，例如，我们从/a导航到一个相对路径b，如果没有配置append，则路径为/b，如果配了，则为/a/b

```
<router-link to="b" append>Home</router-link>
```

4、tag

　　有时候想要<router-link>渲染成某种标签，例如<li>。于是我们使用tag prop 类指定何种标签，同样它还是会监听点击，触发导航。

```
<router-link to="/foo" tag="li">FOO</router-link>
// 渲染结果 
<li>FOO</li>
```

5、active-class

　　设置链接激活时使用的css类名。默认值可以通过路由的构造选项linkActiveClass来全局配置, 默认值为 ‘router-link-active‘

```
export default New Router({
   linkActiveClass: ‘active‘ 
})
```

6、exact

　　"是否激活"，默认是false 。举个粟子，如果当前的路径是/a 开头的，那么<router-link to="/a"> 也会被设置css类名

　　按照这个规则，<router-link to="/"> 将会点亮各个路由！想要链接使用"exact匹配模式"，则使用exact属性：

```
// 这个链接只会在地址为 / 的时候被激活 
<router-link to="/" exact>Home</router-link>

<router-link to="/user">USER</router-link>

<router-link to="/user/userinfo">USER-info</router-link>

// 如果不设置exact，则当路由到了/user/userinfo 页面时，USER也是被设置了router-link-active样式的！
```



7、events

　　声明可以用来触发导航的事件（默认是‘click‘）。可以是一个字符串或者是一个包含字符串的数组

8、将"激活时的css类名"应用在外层元素

　　有时候我们要让"激活的css类名"应用在外层元素，而不是<a>标签本身，那么可以用<router-link>渲染外层元素，包裹着内层的原生<a>标签

```
<router-link tag="li" to="/foo">
    <a>/foo</a>
</router-link>
//在这种情况下，<a>将作为真实的链接（能获取到正确的href的），而"激活时的css类名"则设置到外层的<li>
```

9、方法

　　router-link默认是触发router.push(location)，如果设置的replace 则触发router.replace(location)，这有啥区别呢？

　　router.push() ：导航跑到不同的URL,这个方法会向history栈添加一个新的记录，所以，当用户点击浏览器后退按钮时，则回到之前的url.

　　router.replace(): 跟router.push作用是一样的，但是，它不会向history添加新记录，而是跟它的方法名一样替换掉当前的history记录.

　　router.go(n): 这个方法的参数是一个整数，意思是在history记录中向前或者后退多少步，类似window.history.Go(n)