1 路由：
注意：项目在初始化的时候不要继承vue-router

2 安装路由：
cnpm install vue-router --save

3 导入路由文件：
如果使用模板安装vurrouter ，需要在main.js文件里面引入VueRouter文件并且通过使用Vue.use()明确地安装路由
import VueRouter from 'vue-router';
Vue.use(VueRouter);

4 创建路由：
// 使用vue.use函数明确地安装路由
Vue.use(VueRouter);
Vue.config.productionTip = false;
// 创建路由实例对象
const router = new VueRouter({
    // 匹配路由
    routes:[
        {
            path:"/",
            name:"helloworld",
            component:"helloworld"
        }
    ]
});

5 给出路由显示的位置：
在app.vue使用router-view标签

6 将创建的路由对象注入vue实例中：

@表示定位到根目录

7.路由跳转： router-link   属性tag规定标签元素

8，动态路由（传递参数）   
1-在路由入口文件router.js里面的path去添加参数路径  2-在app.vue的to路径后台加上参数  3-在动态路由组件里接收参数


9，路由的嵌套规则：
1-先在路由入口文件去配置import 和路由信息
2-去父级路由设置router-link 和 router-view
    <ul>
      <router-link tag="li" to="/About/base">base</router-link>
      <router-link tag="li" to="/About/http">http</router-link>
      <!-- <router-link tag="li" to="/父路由/子路由">子路由</router-link> 跳转的时候如果不写父级路由地址会跳转失败-->
    </ul>
    <router-view></router-view>

10，编程式导航：
1-router.push
<button @click="gotoBase" type="button" name="button">去base</button>
gotoBase() {
  // this.$router.push('/base');//直接写路径
  this.$router.push({path:"/About/base"});//写参数 push函数的效果，没访问一次页面，就在history堆栈新生成一条记录
}
2-router.replace()不会向history添加新记录，而是替换掉当前的记录
3-router.go(n)  参数n为整数，意思是在history记录中向前或向后退多少步，类似window.history.go(n)

11,通过name属性确定路由跳转链接=：在路由入口文件router.js中找到相应路由的name属性值，在需要被嵌套的父路由中设置
 <!--通过name属性确定路由跳转链接====命名的路由======主要掌握============ -->
      <router-link tag="li" v-bind:to="{name:'base',params:{id:baseparams}}}">base</router-link>
      <router-link tag="li" :to="{name:'http'}">http</router-link>

      data() {
        return {
          baseparams:"basedemo",
          clickparams:"点击1下就传递字符串参数啦"
        }
      },
      methods:{
          gotoBase() {
            this.$router.push({name:'base',params:{id:this.clickparams}});//？？======点击后,base路由的id值就成了clickparams的值=======命名的路     由 通过路由的name属性，定位到需要切换到的路由是具体哪一个
            console.log(this.$route.name);// String类型   如果当前路由已经是base路由，那么就不跳转路由，而是跳出提示框
            console.log(this.$route.name == 'base');//为什么永远是true??因为当前路由的name属性一直是base，即使已经切换到了http路由，当前路由的name属性还是base 为什么？？？？？
            if(this.$route.name == 'base' ) {
              alert("当前路由已是base，不要跳转啦");
            }
          }
      }

12,带查询参数：路径会变成/register?plan=private
router.push({path:'register',query:{plan:'private'}})   

13，重定向和别名：在路由入口文件router.js中
==========重定向可以用来：
{//也可以当页面错误找不到页面的时候给一个404错误页面
    path:'*',
    // 重定向
    component: NotFound
},
{//在刷新页面的时候给定一个默认显示的页面；
    path:'/',
    // 重定向 显示的是页面刷新后最开始显示的页面 将页面从根目录页面重定向到hellowworld页面  注意看地址栏的变化
    redirect:'/HelloWorld'
},
==========别名:当用户访问/a时，路径会保持为/a，但是路由匹配的是/b。实际上用户看到的是/b，但其实用户访问的还是/a
const router = new VueRouter ({
    routes:[
        {
            path: '/a',
            component: A,
            alias: '/b'
        }
    ]
})

14,$route.path  获取路由的路径：  例子见http.vue     getcurrenturl

15,路由的高亮：linkActiveClass   router-link-exact-active   router-link-active  在路由入口文件router.js中重新设置处理高亮的类名,在主入口文件app.vue中设置类名样式：
linkActiveClass:"active",//被选中的路由的类名属性，vue自带的,默认是router-link-exact-active。此处意思===》将被选中时的类名由router-link-exact更改为active

/* 全局配置 &.router-link-active{
    color: red;
} */
为什么页面已经重定向了，home组件会一直默认有该类名router-link-active？？？？？？？？？？？？？？？？？？？？

/* 全局配置 精准匹配 &.router-link-exact-active{
    color: red;
    border:1px solid green;
} */


10，vue中 $router和$route的区别：
